# coding: utf8

import lxml.html as html
import os

import requests



def run(path):

    if not  os.path.exists(path + '/texas'):
        print ('Директория ' + path +'/texas' + ' не существует')
        print ('Создаю...')
        os.mkdir(path + '/texas')

    headers = {'User-Agent' : 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0', 'Referrer' : ''}


    destination = path + '/texas/'
    download_url = 'http://www.texas-garden.com'
    main_domain_stat = 'http://www.texas-garden.com/en/spareparts/'
    page = html.parse(main_domain_stat)

    #node = page.xpath('/html/body')
    e = page.getroot().\
            find_class('sparepartlistcontainer').\
            pop()
    print (e.get('class'))

    nodes = e.xpath('./a/@href') # Открываем раздел
    for node in nodes: # Перебираем элементы
         print node
         page2 = html.parse(main_domain_stat + node)
         nodes2 = page2.xpath(".//*[@class='sparepartlistcontainer']/a/@href")
         for node2 in nodes2:
             print ( "--  " + node2)
             page3 = html.parse(download_url + node2)
             nodes3 = page3.xpath(".//*[@class='sparepartlistcontainer']/a/@href")
             for node3 in nodes3:
                 print ("----- " +node3)

                 page4 = html.parse(download_url + node3)

                 try:
                    drawing = page4.xpath(".//*[@class='sparepartdrawing']/a/@href")[0]
                    filename = drawing.rsplit('/', 1)[1]
                 except:
                     print ("nothing to download")
                 print ("--------- " + drawing.rsplit('/', 1)[1])
                 print ("--------- " + drawing)

                 headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',
                            'Content-type': 'application/pdf'}
                 session = requests.session()
                 resp = session.get(drawing, headers=headers)
                 try:
                    #print resp.text
                    #page5 = html.parse(drawing)
                    #print page5
                    #urllib.urlretrieve(drawing , destination + filename)
                    #pdf = urllib.urlopen(drawing).read()
                    f = open(destination + filename, "wb")
                    f.write(resp.content)
                    f.close()


                 except:
                     print ("wrong file name")
         #print(download_url + node.get('value'))
         #print(node.text)
         #urllib.urlretrieve(download_url + node.get('value'), destination + node.text)

if __name__ == '__main__':
    path = os.getcwd()
    print (path)
    run(path)