# coding: utf8

import lxml.html as html
import urllib, os


def run(path):
    if not os.path.exists(path + '/prorab'):
        print ('Директория ' + path + '/prorab' + ' не существует')
        print ('Создаю...')
        os.mkdir(path + '/prorab')
    destination = path + '/prorab/'
    download_url = 'http://www.prorabtools.ru'
    main_domain_stat = 'http://www.prorabtools.ru/service/'
    page = html.parse(main_domain_stat)

    #node = page.xpath('/html/body')
    e = page.getroot().\
            find_class('medium-5 large-5 columns').\
            pop()
    print (e)
    t = e.getchildren().pop()
    print(t)
    #nodes = t.xpath('/') # Открываем раздел
    for node in t: # Перебираем элементы
         #print node.tag,node.keys(),node.values()
         print(download_url + node.get('value'))
         print(node.text)
         urllib.urlretrieve(download_url + node.get('value'), destination + node.text)

if __name__ == '__main__':
    path = os.getcwd()
    print (path)
    run(path)
